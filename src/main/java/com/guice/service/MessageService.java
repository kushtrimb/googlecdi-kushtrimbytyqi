package com.guice.service;

/**
 * Created by Kushtrim on 5/13/2017.
 */
public interface MessageService {

    boolean sendMessage(String msg, String receipient);
}
