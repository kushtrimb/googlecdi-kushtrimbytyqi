package com.guice.service;

import com.google.inject.Singleton;

/**
 * Created by Kushtrim on 5/13/2017.
 */
@Singleton
public class MyService implements MessageService {

    public boolean sendMessage(String msg, String receipient) {
        //some complex code to send Facebook message
        System.out.println("Message sent to Facebook user "+receipient+" with message="+msg);
        return true;
    }

}