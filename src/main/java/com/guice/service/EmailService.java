package com.guice.service;

import com.google.inject.Singleton;

/**
 * Created by Kushtrim on 5/13/2017.
 */
@Singleton
public class EmailService implements MessageService {

    public boolean sendMessage(String msg, String receipient) {
        //some fancy code to send email
        System.out.println("Email Message sent to "+receipient+" with message="+msg);
        return true;
    }

}